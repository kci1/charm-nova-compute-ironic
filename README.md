## Overview

This charm configures a nova compute node for Ironic which can be used
to provision bare-metal nodes with OpenStack.

## Configuration

Create `options.yaml` file with the config option needed to enable the 
OpenStack Newton (or other release if interested) repository:

    nova-compute-ironic:
      openstack-origin: "cloud:xenial-newton"

## Usage

Deploy the charm and add the relations with the other OpenStack charms:

    juju deploy --config options.yaml cs:~cloudbaseit/xenial/nova-compute-ironic
    juju add-relation nova-compute-ironic rabbitmq-server
    juju add-relation nova-compute-ironic nova-cloud-controller

To scale out horizontally:

    juju add-unit nova-compute-ironic -n <number_of_units>

To scale down:

    juju destroy-unit nova-compute-ironic/<unit_number>


## To deploy locally

    $ juju deploy $path_to_this_checkout --series xenial