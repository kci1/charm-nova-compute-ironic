#
# Copyright 2015 Cloudbase Solutions SRL
#

from charmhelpers.core.hookenv import (
    config,
    log,
    relation_set,
    status_set
)

from charmhelpers.core.host import (
    restart_on_change,
)
from charmhelpers.fetch import (
    apt_install,
    apt_update,
)
from charmhelpers.contrib.openstack.utils import (
    configure_installation_source,
    openstack_upgrade_available,
)
from nova_ironic_utils import (
    register_configs,
    cmd_all_services,
    restart_map,
    do_openstack_upgrade,
)
import constants

CONFIGS = register_configs()


def install():
    configure_installation_source(config('openstack-origin'))
    apt_update()
    apt_install(constants.PACKAGES, fatal=True)


@restart_on_change(restart_map())
def config_changed():
    if openstack_upgrade_available('nova-common'):
        do_openstack_upgrade(CONFIGS)
    CONFIGS.write_all()


def start():
    cmd_all_services('start')


def stop():
    cmd_all_services('stop')


def amqp_joined(relation_id=None):
    relation_set(relation_id=relation_id,
                 username=config('rabbit-user'),
                 vhost=config('rabbit-vhost'))


@restart_on_change(restart_map())
def amqp_changed():
    if 'amqp' not in CONFIGS.complete_contexts():
        log('amqp relation incomplete. Peer not ready?')
        return
    CONFIGS.write_all()


@restart_on_change(restart_map())
def ironic_api_changed(relation_id=None):
    if 'ironic-api' not in CONFIGS.complete_contexts():
        log('ironic-api relation incomplete. Peer not ready?')
        return
    CONFIGS.write_all()
    status_set('active', 'Ready.')


@restart_on_change(restart_map())
def cloud_compute_changed():
    CONFIGS.write_all()


@restart_on_change(restart_map())
def image_service_changed():
    if 'image-service' not in CONFIGS.complete_contexts():
        log('image-service relation incomplete. Peer not ready?')
        return
    CONFIGS.write_all()


@restart_on_change(restart_map())
def neutron_api_changed():
    if 'neutron-api' not in CONFIGS.complete_contexts():
        log('neutron-api relation is incomplete.')
        return
    CONFIGS.write_all()


@restart_on_change(restart_map())
def identity_service_changed():
    if 'identity-service' not in CONFIGS.complete_contexts():
        log('identity-service relation is incomplete.')
        return
    CONFIGS.write_all()


@restart_on_change(restart_map())
def relation_broken():
    CONFIGS.write_all()


def identity_joined(relation_id=None):
    relation_data = {
        'service': 'nova-compute-ironic',
        'region': config('region')
    }
    relation_set(relation_id=relation_id, **relation_data)
