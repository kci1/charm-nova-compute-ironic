#
# Copyright 2015 Cloudbase Solutions SRL
#

from charmhelpers.contrib.openstack import (
    templating,
    context,
)
from charmhelpers.contrib.openstack.utils import (
    os_release,
    get_os_codename_install_source,
    configure_installation_source,
)
from charmhelpers.core.host import (
    service,
    service_start,
    service_running,
)
from charmhelpers.core.hookenv import (
    config,
    log,
)
from charmhelpers.fetch import apt_update, apt_upgrade
from nova_ironic_context import (
    CloudComputeContext,
    HostIPContext,
    IronicContext,
)
import constants


def resource_map():
    resource_map = {
        constants.NOVA_CONF: {
            'services': ['nova-compute'],
            'contexts': [context.LogLevelContext(),
                         CloudComputeContext(),
                         HostIPContext(),
                         IronicContext(),
                         context.IdentityServiceContext(),
                         context.ImageServiceContext(),
                         context.AMQPContext()]
        },
        constants.NOVA_COMPUTE_CONF: {
            'services': ['nova-compute'],
            'contexts': [IronicContext()]
        }
    }

    return resource_map


def register_configs():
    '''
    Returns an OSTemplateRenderer object with all required configs registered.
    '''
    release = os_release('nova-common')
    configs = templating.OSConfigRenderer(
                                        templates_dir=constants.TEMPLATES_DIR,
                                        openstack_release=release)

    for cfg, d in resource_map().iteritems():
        configs.register(cfg, d['contexts'])

    return configs


def restart_map():
    return {k: v['services'] for k, v in resource_map().iteritems()}


def services():
    ''' Returns a list of services associated with this charm '''
    _services = []
    for v in restart_map().values():
        _services = _services + v
    return list(set(_services))


def cmd_all_services(cmd):
    if cmd == 'start':
        for svc in services():
            if not service_running(svc):
                service_start(svc)
    else:
        for svc in services():
            service(cmd, svc)


def do_openstack_upgrade(configs):
    new_src = config('openstack-origin')
    new_os_rel = get_os_codename_install_source(new_src)

    log('Performing OpenStack upgrade to %s.' % (new_os_rel))

    configure_installation_source(new_src)
    dpkg_opts = [
        '--option', 'Dpkg::Options::=--force-confnew',
        '--option', 'Dpkg::Options::=--force-confdef',
    ]
    apt_update()
    apt_upgrade(options=dpkg_opts, fatal=True, dist=True)

    # set CONFIGS to load templates from new release
    configs.set_release(openstack_release=new_os_rel)
    configs.write_all()
