#
# Copyright 2015 Cloudbase Solutions SRL
#

PACKAGES = ['nova-compute', 'python-ironicclient']
TEMPLATES_DIR = 'templates/'
NOVA_CONF_DIR = "/etc/nova"
NOVA_CONF = '%s/nova.conf' % NOVA_CONF_DIR
NOVA_COMPUTE_CONF = '%s/nova-compute.conf' % NOVA_CONF_DIR
