#
# Copyright 2015 Cloudbase Solutions SRL
#

from charmhelpers.contrib.openstack.utils import (
    get_host_ip,
)
from charmhelpers.contrib.openstack import (
    context,
    neutron,
)
from charmhelpers.core.hookenv import (
    config,
    relation_get,
    relation_ids,
    related_units,
    log,
    is_relation_made,
    unit_get,
)
from charmhelpers.contrib.network.ip import (
    get_ipv6_addr,
    format_ipv6_addr,
)


class CloudComputeContext(context.OSContextGenerator):
    interfaces = ['cloud-compute']

    @property
    def network_config(self):
        settings = ['network-manager', 'neutron-plugin', 'quantum-plugin',
                    'neutron-url', 'quantum-url']

        net_config = {}
        for rid in relation_ids('cloud-compute'):
            for unit in related_units(rid):
                for setting in settings:
                    value = relation_get(setting, rid=rid, unit=unit)
                    if value:
                        net_config[setting] = value
        return net_config

    @property
    def network_manager(self):
        return neutron.network_manager()

    @property
    def neutron_plugin(self):
        return self.network_config.get('neutron-plugin') or \
            self.network_config.get('quantum_plugin')

    @property
    def neutron_url(self):
        return self.network_config.get('neutron-url') or \
            self.network_config.get('quantum_url')

    def restart_trigger(self):
        rt = None
        for rid in relation_ids('cloud-compute'):
            for unit in related_units(rid):
                rt = relation_get('restart_trigger', rid=rid, unit=unit)
                if rt:
                    return rt

    def neutron_context(self):
        neutron_ctxt = {'neutron_url': None}
        for rid in relation_ids('neutron-api'):
            log('rid: %s' % rid)
            for unit in related_units(rid):
                log('unit: %s' % unit)
                rdata = relation_get(rid=rid, unit=unit)
                log('rdata: %s' % rdata)
                url = rdata.get('quantum_url') or rdata.get('neutron-url')
                if url is not None:
                    neutron_ctxt = {
                        'neutron_url': url,
                    }

        missing = [k for k, v in neutron_ctxt.iteritems() if v in ['', None]]
        if missing:
            log('Missing required relation settings for Neutron: ' +
                ' '.join(missing))
            return {}

        return neutron_ctxt

    def __call__(self):
        ctxt = {'network_manager': self.network_manager}
        if self.network_manager == 'neutron':
            ctxt['network_manager_config'] = self.neutron_context()
        if self.restart_trigger():
            ctxt['restart_trigger'] = self.restart_trigger()
        return ctxt


class HostIPContext(context.OSContextGenerator):
    def __call__(self):
        ctxt = {}
        if config('prefer-ipv6'):
            host_ip = get_ipv6_addr()[0]
        else:
            host_ip = get_host_ip(unit_get('private-address'))

        if host_ip:
            ctxt['host_ip'] = format_ipv6_addr(host_ip) or host_ip

        return ctxt


class IronicContext(context.OSContextGenerator):
    interfaces = ['ironic-api']

    def __call__(self):
        for rid in relation_ids('ironic-api'):
            for unit in related_units(rid):
                rdata = relation_get(rid=rid, unit=unit)
                ironic_config = {
                    'admin_user': rdata.get('admin_user'),
                    'admin_password': rdata.get('admin_password'),
                    'auth_protocol': rdata.get('auth_protocol') or 'http',
                    'auth_host': rdata.get('auth_host'),
                    'auth_port': rdata.get('auth_port'),
                    'admin_tenant_name': rdata.get('admin_tenant_name'),
                    'api_protocol': rdata.get('api_protocol') or 'http',
                    'api_host': rdata.get('api_host'),
                    'api_port': rdata.get('api_port'),
                }
                if context.context_complete(ironic_config):
                    return {'ironic_config': ironic_config,
                            'compute_driver': 'ironic.IronicDriver'}
        return {}
